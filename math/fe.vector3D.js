class feVector3D {
  constructor(x, y, z) {
    if (isNaN(x)) {
      this.x = 0;
    } else {
      this.x = x;
    }
    if (isNaN(y)) {
      this.y = 0;
    } else {
      this.y = y;
    }
    if (isNaN(z)) {
      this.z = 0;
    } else {
      this.z = z;
    }
  }
  normalize() {
    var length = this.length();
    if (length != 0) {
      this.x = this.x / length;
      this.y = this.y / length;
      this.z = this.z / length;
    }
  }
  rotateX(origin, angle) {
    let tmp = [], rotated = [];
    //Translate point to the origin
    tmp[0] = this.x - origin.x;
    tmp[1] = this.y - origin.y;
    tmp[2] = this.z - origin.z;

    //perform rotation
    rotated[0] = tmp[0];
    rotated[1] = tmp[1] * Math.cos(angle) - tmp[2] * Math.sin(angle);
    rotated[2] = tmp[1] * Math.sin(angle) + tmp[2] * Math.cos(angle);

    //translate to correct position
    this.x = rotated[0] + origin.x;
    this.y = rotated[1] + origin.y;
    this.z = rotated[2] + origin.z;
  }

  rotateY(origin, angle) {
    let tmp = [], rotated = [];
    //Translate point to the origin
    tmp[0] = this.x - origin.x;
    tmp[1] = this.y - origin.y;
    tmp[2] = this.z - origin.z;
    //perform rotation
    rotated[0] = tmp[2] * Math.sin(angle) + tmp[0] * Math.cos(angle);
    rotated[1] = tmp[1];
    rotated[2] = tmp[2] * Math.cos(angle) - tmp[0] * Math.sin(angle);
    //translate to correct position
    this.x = rotated[0] + origin.x;
    this.y = rotated[1] + origin.y;
    this.z = rotated[2] + origin.z;
  }

  rotateZ(origin, angle) {
    let tmp = [], rotated = [];
    //Translate point to the origin
    tmp[0] = this.x - origin.x;
    tmp[1] = this.y - origin.y;
    tmp[2] = this.z - origin.z;
    //perform rotation
    rotated[0] = tmp[0] * Math.cos(angle) - tmp[1] * Math.sin(angle);
    rotated[1] = tmp[0] * Math.sin(angle) + tmp[1] * Math.cos(angle);
    rotated[2] = tmp[2];
    //translate to correct position
    this.x = rotated[0] + origin.x;
    this.y = rotated[1] + origin.y;
    this.z = rotated[2] + origin.z;
  }
}

function angle(vec, vec2) {
  var tmp1 = new feVector3D(vec.x, vec.y, vec.z);
  var tmp2 = new feVector3D(vec2.x, vec2.y, vec2.z);
  tmp1.normalize();
  tmp2.normalize();
  let cosine = dot(tmp1, tmp2);
  if (cosine > 1.0) {
    return 0;
  }
  else if (cosine < -1.0) {
    return Degree(Math.PI);
  } else {
    return Degree(Math.acos(cosine));
  }
}

function length(vec) {
  return Math.sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
}

function dot(vec1, vec2) {
  return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
}

function normalize(vec) {
  var final = new feVector3D();
  var len = length(vec);
  if (len != 0) {
    final.x = vec.x / len;
    final.y = vec.y / len;
    final.z = vec.z / len;
  }
  return final;
}

function add(vec1, vec2) {
  var final = new feVector3D();
  final.x = vec1.x + vec2.x;
  final.y = vec1.y + vec2.y;
  final.z = vec1.z + vec2.z;
  return final;
}

function subtract(vec1, vec2) {
  var final = new feVector3D();
  final.x = vec1.x - vec2.x;
  final.y = vec1.y - vec2.y;
  final.z = vec1.z - vec2.z;
  return final;
}

function cross(vec1, vec2) {
  var final = new feVector3D();
  final.x = vec1.y * vec2.z - vec1.z * vec2.y;
  final.y = vec1.z * vec2.x - vec1.x * vec2.z;
  final.z = vec1.x * vec2.y - vec1.y * vec2.x;
  return final;
}

function multiply(vec1, n) {
  var final = new feVector3D();
  final.x = vec1.x * n;
  final.y = vec1.y * n;
  final.z = vec1.z * n;
  return final;
}

function divide(n) {
  var final = new feVector3D();
  final.x = vec1.x / n;
  final.y = vec1.y / n;
  final.z = vec1.z / n;
  return final;
}

function cross(vec1, vec2) {
  var final = new feVector3D();
  final.x = vec1.y * vec2.z - vec1.z * vec2.y;
  final.y = vec1.z * vec2.x - vec1.x * vec2.z;
  final.z = vec1.x * vec2.y - vec1.y * vec2.x;
  return final;
}
