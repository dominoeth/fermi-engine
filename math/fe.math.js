function Degree(radian)
{
  return radian * (180 / Math.PI);
}
function Radian(degree)
{
  return degree * (Math.PI / 180);
}
function intersect(line1Start, line1End, line2Start, line2End)
{

  /* straight slope */
  var s1_x, s1_y, s2_x, s2_y;
  s1_x = line1End.x - line1Start.x;
  s1_y = line1End.y - line1Start.y;

  s2_x = line2End.x - line2Start.x;
  s2_y = line2End.y - line2Start.y;

  var s, t;
  /* magic */
  s = (-s1_y * (line1Start.x - line2Start.x) + s1_x * (line1Start.y - line2Start.y)) / (-s2_x * s1_y + s1_x * s2_y);
  t = ( s2_x * (line1Start.y - line2Start.y) - s2_y * (line1Start.x - line2Start.x)) / (-s2_x * s1_y + s1_x * s2_y);

  /* if intersection is there, add to list */
  if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
  {
    var newPoint = new Point();
    newPoint.x = line1Start.x + (t * s1_x);
    newPoint.y = line1Start.y + (t * s1_y);
    /* return a new point object */
    return newPoint;
  }
  /* return false if no intersection is given */
  return false;
}

function getClosestPoint(origin, points /* list */)
{
  var final = new Point();
  var distance = 1000000;
  for (var point in points) {
    if (points.hasOwnProperty(point)) {
      var length = Math.sqrt((points[point].x - origin.x) * (points[point].x - origin.x) + (points[point].y - origin.y) * (points[point].y - origin.y));
      if(length <= distance)
      {
        final.x = points[point].x;
        final.y = points[point].y;
        distance = length;
      }
    }
  }
  return final;
}

function angleFrom(point, center)
{
  var dy = point.y - center.y;
  var dx = point.x - center.x;
  var angle = -Math.atan2(dy,dx);

  return Degree(angle);
}

function clockWiseCheck(pointA,pointB,center)
{
  if (pointA.x - center.x >= 0 && pointB.x - center.x < 0)
      return true;
  if (pointA.x - center.x < 0 && pointB.x - center.x >= 0)
      return false;
  if (pointA.x - center.x == 0 && pointB.x - center.x == 0) {
      if (pointA.y - center.y >= 0 || pointB.y - center.y >= 0)
          return pointA.y > pointB.y;
      return pointB.y > pointA.y;
  }

  /* compute the cross product of vectors (center -> a) x (center -> b) */
  var det = (pointA.x - center.x) * (pointB.y - center.y) - (pointB.x - center.x) * (pointA.y - center.y);
  if (det < 0)
      return true;
  if (det > 0)
      return false;

  /* points a and b are on the same line from the center
     check which point is closer to the center */
  var d1 = (pointA.x - center.x) * (pointA.x - center.x) + (pointA.y - center.y) * (pointA.y - center.y);
  var d2 = (pointB.x - center.x) * (pointB.x - center.x) + (pointB.y - center.y) * (pointB.y - center.y);
  return d1 > d2;
}

function rotate(point,origin, rad)
{
  var newPoint = new Point();
  var cx = origin.x;
  var cy = origin.y;
  var x = point.x;
  var y = point.y;

  var cos = Math.cos(rad);
  var sin = Math.sin(rad);
  var nx = (cos * (x - cx)) + (sin * (y - cy)) + cx;
  var ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;

  newPoint.x = nx;
  newPoint.y = ny;

  return newPoint;
}
