class feQuaternion {
  constructor(x, y, z, w) {
    this.x = x || 0; /* x pos of the vector */
    this.y = y || 0; /* y pos of the vector */
    this.z = z || 0; /* z pos of the vector */
    this.w = w || 0; /* w rotation/scalar of the vector */
    this._length = 0; /* for normalize this var will be pre allocated, faster than creating a var each time */
  }
  length() {
    return sqrt(this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
  }
  normalize() {
    this._length = length();
    this.x /= this._length();
    this.y /= this._length();
    this.z /= this._length();
    this.w /= this._length();
  }
  multiply(quat) {
    this.x = this.w * quat.x + this.x * quat.w + this.y * quat.z - this.z * quat.y;
    this.y = this.w * quat.y - this.x * quat.z + this.y * quat.w + this.z * quat.x;
    this.z = this.w * quat.z + this.x * quat.y - this.y * quat.x + this.z * quat.w;
    this.w = this.w * quat.w - this.x * quat.x - this.y * quat.y - this.z * quat.z;
  }
  conjungate() {
    this.x = -this.x;
    this.y = -this.y;
    this.z = -this.z;
    this.w = -this.w;
  }
}