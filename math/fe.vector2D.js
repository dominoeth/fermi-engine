class feVector2D {
  constructor(x, y, rotation) {
    if (isNaN(x)) {
      this.x = 0;
    } else {
      this.x = x;
    }
    if (isNaN(y)) {
      this.y = 0;
    } else {
      this.y = y;
    }
    this.z = 1;
  }
  normalize() {
    var length = this.length();
    if (length != 0) {
      this.x = this.x / length;
      this.y = this.y / length;
    }
  }
  length() {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }
  scalar(vector) {
    return this.x * vector.x + this.y * vector.y;
  }
  add(vector) {
    var newVector = new Vector(0, 0);
    newVector.x = vector.x + this.x;
    newVector.y = vector.y + this.y;
    return newVector;
  }
  multiply(n) {
    this.x *= n || 0;
    this.y *= n || 0;
  }
  divide(n) {
    this.x /= n || 0;
    this.y /= n || 0;
  }
}
