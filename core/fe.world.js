class feWorld {
  constructor() {
    this.ambientLight = null;
    this.pointLights = [];
    this.spotLights = [];
    this.directionalLights = [];
    this.objects = [];
  }

  initialize() {
    var ambient = new feAmbientLight();
    ambient.setRGB(0.0, 0.0, 0.0);
    this.add(ambient);

    var point = new fePointLight();
    point.setRGB(1.0, 1.0, 1.0);
    this.add(point);
  }

  add(object) {
    if (object instanceof feLight) {
      if (object instanceof feAmbientLight) {
        this.ambientLight = object;
      }
      if (object instanceof fePointLight) {
        this.pointLights.push(object);
      }
    } else {
      if (object instanceof feAsset) {
        this.objects.push(object);
      } else if (object instanceof feEntity) {
        this.objects.push(object);
      }
    }
  }
  remove(object) {
    if (object instanceof feAsset) {
      for(var i = 0; i < this.objects.length; i ++) {
        if(this.objects[i] == object) {
          this.objects.splice(i,1);
          break;
        }
      }
    }
  }

  update(dt) {
    for (var i = 0; i < this.objects.length; i++) {

      this.objects[i].update(dt);

    }
  }
}
