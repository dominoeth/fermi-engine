class feEventHandler {
    static attach(object) {
        object.on = function (eventName, fn) {
            if (typeof this.events[eventName] === 'undefined') {
                this.events[eventName] = []
            }
            this.events[eventName].push(fn);
        }
        object.off = function (eventName, fn) {

            // check for wildcard *
            if (eventName === '*' || typeof eventName === 'undefined') {
                for (var k in this.events) {
                    if (this.events.hasOwnProperty(k)) {
                        if (typeof this.events[k] !== 'undefined') {
                            // if no function or wildcard was given, we are removing all handlers for this event
                            if (typeof fn === 'undefined' || fn === '*') {
                                this.events[k] = [];
                            }
                            // else we try and remove just that function
                            else {
                                for (var i = 0; i < this.events[k].length; i++) {
                                    if (this.events[k][i] === fn) {
                                        this.events[k].splice(i, 1);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                if (typeof this.events[eventName] !== 'undefined') {
                    // if no function or wildcard was given, we are removing all handlers for this event
                    if (typeof fn === 'undefined' || fn === '*') {
                        this.events[eventName] = [];
                    }
                    // else we try and remove just that function
                    else {
                        for (var i = 0; i < this.events[eventName].length; i++) {
                            if (this.events[eventName][i] === fn) {
                                this.events[eventName].splice(i, 1);
                                break;
                            }
                        }
                    }
                }
            }
        }
        object.trigger = function (eventName) {
            var leftovers = Array.prototype.slice.apply(arguments, [1]);

            if (eventName === '*') {
                for (var k in this.events) {
                    if (this.events.hasOwnProperty(k)) {
                        if (typeof this.events[k] !== 'undefined') {
                            for (var i = 0; i < this.events[k].length; i++) {
                                this.events[k][i].apply(this, leftovers);
                            }
                        }
                    }
                }
            }
            else {
                if (typeof this.events[eventName] !== 'undefined') {
                    for (var i = 0; i < this.events[eventName].length; i++) {
                        this.events[eventName][i].apply(this, leftovers);
                    }
                }
            }
        }
        object.events = {};
    }
}



