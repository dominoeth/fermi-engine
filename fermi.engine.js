/* engine root */
class FermiEngine {
  constructor() {
    this.animationCallbackFunction = null;
    this.isInitialized = false;

    loader.addFile("../../fermi-engine/shader/fe.basicFragmentShader.fs");
    loader.addFile("../../fermi-engine/shader/fe.basicVertexShader.vs");
    loader.addFile("../../fermi-engine/shader/lines/fe.basicLineFragmentShader.fs");
    loader.addFile("../../fermi-engine/shader/lines/fe.basicLineVertexShader.vs");
    loader.addFile("../../fermi-engine/shader/fbo/fe.basicFBOFragmentShader.fs");
    loader.addFile("../../fermi-engine/shader/fbo/fe.basicFBOVertexShader.vs");
    loader.addFile("girl.obj");  
    loader.load();

    loader.on("finished", function () {
      loader.off("finished");
      engine.initialize();

      LoadContent();
      Initialize();
    });

  }

  initialize() {
    /* initialize renderer */

    renderer.initialize(document.getElementById("playground"));
    renderer.initShader();
    renderer.initBuffer();
    renderer.initTextures();

    /* initialize camera */
    camera.initialize();

    camera.setPosition(new feVector3D(0, -5, -10));

    world.initialize();

    /* global functions */


    this.isInitialized = true;
  }

  /* entry point of the game engine */
  run() {
    requestAnimationFrame(this.run.bind(this));
    if (!this._checkForInit()) return false;

    perfomanceCounter.stop();

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.enable(gl.DEPTH_TEST);

    world.update(perfomanceCounter.getDeltaTime());
    camera.update(perfomanceCounter.getDeltaTime());

    Draw();
    perfomanceCounter.update();

    this.animationCallbackFunction(perfomanceCounter.getDeltaTime());
    renderer.render();
    perfomanceCounter.start();
  }

  setAnimationFunction(callbackFunction) {
    this.animationCallbackFunction = callbackFunction;
  }

  _checkForInit() {
    return this.isInitialized;
  }
}
