class feTexture {
  constructor(textureName) {
    this.isFinishedLoading = false;
    this.texture = gl.createTexture();
    if(textureName != "" && textureName != null && textureName != undefined)  {
      var self = this;
      this.texture.image = new Image();
      this.texture.image.crossOrigin = "anonymous";
      this.texture.image.onload = function() {
        self.handleLoad();
      }
      this.texture.image.src = textureName;
    }
  }

  bind() {
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
  }
  unbind() {
    gl.activeTexture(null);
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
  }
  
  handleLoad() {
    gl.bindTexture(gl.TEXTURE_2D, this.texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.texture.image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    // gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.bindTexture(gl.TEXTURE_2D, null);
    this.isFinishedLoading = true;
  }
}
