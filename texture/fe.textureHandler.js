class feTextureHandler {
  constructor() {
    feEventHandler.attach(this);
    this.textures = [];
    this.textureNames = [];
  }

  addTexture(name) {
    this.textureNames.push(name);
  }

  loadTextures() {
    var textureName = "";
    var texture = null;
    for(var i = 0; i < this.textureNames.length; i ++) {
      textureName = this.textureNames[i];
      texture = new feTexture(textureName);
      this.textures.push(texture);
    }
    this.trigger("finishedLoad");
  }
  get(name) {
    if(name != null) {
      for(var i = 0; i < this.textureNames.length; i ++) {
        if(name == this.textureNames[i]) {
          return this.textures[i];
        }
      }
    }
  }
  getTexture(name) {
    if(name != null) {
      for(var i = 0; i < this.textureNames.length; i ++) {
        if(name == this.textureNames[i]) {
          return this.textures[i];
        }
      }
    }
  }
}  