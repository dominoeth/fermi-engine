var VERTEX = 0;
var NORMALS = 1;
var UV = 2;
var VERTEX_INDICES = 3;
var UV_INDICES = 4;
var NORMALS_INDICES = 5;

class feBufferHandler {
  constructor() {
    this.buffers = [];
    this.bufferNames = [];

    this.verticesBuffer = [];
    this.normalsBuffer = [];
    this.uvsBuffer = [];
    this.vertexIndicesBuffer = [];
    this.uvIndicesBuffer = [];
    this.normalIndicesBuffer = [];
    this.lastBoundBufferID = -1;
  }
  get(bufferName, type) {
    var index = -1;
    for (var i = 0; i < this.bufferNames.length; i++) {
      if (this.bufferNames[i] = bufferName) {
        index = i;
        break;
      }
    }
    if (index == -1) {
      console.error("can´t get buffer for " + bufferName);
      return;
    }
    // if (this.lastBoundBufferID == index) {
    //   return;
    // } else {
    //   this.lastBoundBufferID = index;
    // }

    if (type == VERTEX) {
      return this.verticesBuffer[index];
    }
    if (type == NORMALS) {
      return this.normalsBuffer[index];
    }
    if (type == UV) {
      return this.uvsBuffer[index];
    }
    if (type == VERTEX_INDICES) {
      return this.vertexIndicesBuffer[index];
    }
    if (type == UV_INDICES) {
      return this.uvIndicesBuffer[index];
    }
    if (type == NORMALS_INDICES) {
      return this.normalIndicesBuffer[index];
    }
  }


  bindBuffer(bufferName, type) {
    var index = -1;
    for (var i = 0; i < this.bufferNames.length; i++) {
      if (this.bufferNames[i] = bufferName) {
        index = i;
        break;
      }
    }
    if (index == -1) {
      console.error("can´t get buffer for " + bufferName);
      return;
    }
    if (this.lastBoundBufferID == index) {
      return;
    } else {
      this.lastBoundBufferID = index;
    }

    if (type == VERTEX) {
      this.verticesBuffer[index].bind();
      return;
    }
    if (type == NORMALS) {
      this.normalsBuffer[index].bind();
      return;
    }
    if (type == UV) {
      this.uvsBuffer[index].bind();
      return;
    }
    if (type == VERTEX_INDICES) {
      return this.vertexIndicesBuffer[index].bind();
      return;
    }
    if (type == UV_INDICES) {
      return this.uvIndicesBuffer[index].bind();
      return;
    }
    if (type == NORMALS_INDICES) {
      return this.normalIndicesBuffer[index].bind();
      return;
    }
  }


  createBuffer(bufferName) {
    for (var i = 0; i < this.bufferNames.length; i++) {
      if (this.bufferNames[i] = bufferName) {
        return;  /* buffer is already available */
      }
    }

    var data = ObjLoader.parseObjText(loader.content(bufferName));

    var uvBuffer = new feBuffer(data[3], 2);
    var vertexBuffer = new feBuffer(data[1], 3);
    var vertexNormalBuffer = new feBuffer(data[2], 3);
    var vertexIndicesBuffer = new feBuffer(data[0], 1, gl.ELEMENT_ARRAY_BUFFER, "uint16");

    this.uvsBuffer.push(uvBuffer);
    this.verticesBuffer.push(vertexBuffer);
    this.normalsBuffer.push(vertexNormalBuffer);
    this.vertexIndicesBuffer.push(vertexIndicesBuffer);

    this.uvIndicesBuffer.push(null);
    this.normalIndicesBuffer.push(null);
    this.bufferNames.push(bufferName);
  }
}