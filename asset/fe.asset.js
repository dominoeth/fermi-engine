class feAsset extends feEntity {
  constructor(objFile, textureFile) {
    super();

    this._vertexBuffer = null;
    this._vertexNormalBuffer = null;
    this._vertexIndicesBuffer = null;
    this._textureCoordinatesBuffer = null;
    this._texture = null;
    this._textureName = null;
    this._renderType = gl.TRIANGLES;
    this.objFileName = "";
    this._pickingColor = feAsset.generateColorByID(this.id);

    this.translationMatrix = mat4.create();
    this.normalMatrix = mat3.create();

    this.castShadow = true;
    this.hasAmbientLight = true;
    this.position = new feVector3D();
    this.rotation = new feVector3D();

    if (objFile != null) {
      this.setModel(objFile);
      if (textureFile != null) {
        bufferHandler.createBuffer(objFile);
        this._textureName = textureFile;
        this.setTexture(textureHandler.getTexture(textureFile), bufferHandler.get(objFile, UV));
      }
    }
  }

  setTexture(texture, cordinates) {

    if (!texture instanceof feTexture) {
      console.log("can not set texture");
      return false;
    }
    if (!cordinates instanceof feBuffer) {
      console.log("can not set texture coordinates buffer");
      return false;
    }

    this._texture = "texture";
    this._textureCoordinatesBuffer = cordinates;
  }

  setModel(objFile) {
    // var data = ObjLoader.parseObjText(loader.content(objFile));
    // this._vertexBuffer = new feBuffer(data[1], 3);
    // this._vertexNormalBuffer = new feBuffer(data[2], 3);
    // this._vertexIndicesBuffer = new feBuffer(data[0], 1, gl.ELEMENT_ARRAY_BUFFER, "uint16");
    this.objFileName = objFile;
  }

  update(dt) {

  }

  setPosition(position) {
    this.position = position;
  }

  setRotation(vector3D) {
    this.rotation = vector3D;
  }

  rotate(vector3D) {
    this.rotation = add(this.rotation, vector3D);
  }

  remove() {
    entitys--;
  }

  draw() {
    if (!this._checkValidation()) {
      return false;
    }

    /* calculate positions and rotations */
    this._matrixCalculations();

    /* bind current shader program, in this case standart shader */
    programm.use();

    /* bind vertices etc */
    this._bindBuffers();

    /* light */

    if (world.ambientLight != null) {

      /* set useLight var in shader to true */
      gl.uniform1i(programm.nativeProgram.useLightingUniform, true);
      /* set world ambient light var in shader */
      gl.uniform3f(programm.nativeProgram.ambientColorUniform, world.ambientLight.r, world.ambientLight.g, world.ambientLight.b);

      /* light calculations */
      var lightingDirection = [0, 0, -1];
      var adjustedLD = vec3.create();
      vec3.normalize(adjustedLD, lightingDirection);
      vec3.scale(adjustedLD, adjustedLD, -1);
      /* set light Direction var in shader */
      gl.uniform3fv(programm.nativeProgram.lightingDirectionUniform, adjustedLD);

      gl.uniform3f(programm.nativeProgram.pointLightingLocation,
        world.pointLights[0].position.x,
        world.pointLights[0].position.y,
        world.pointLights[0].position.z
      );

      gl.uniform3f(programm.nativeProgram.pointLightingColor,
        world.pointLights[0].r,
        world.pointLights[0].g,
        world.pointLights[0].b,
      );

    } else {

      /* no light is used; set useLightning to false */
      gl.uniform1i(programm.nativeProgram.useLightingUniform, false);
      gl.uniform3f(programm.nativeProgram.ambientColorUniform, 1.0, 1.0, 1.0);

      var lightingDirection = [1, 0, 0];
      var adjustedLD = vec3.create();
      vec3.normalize(adjustedLD, lightingDirection);
      vec3.scale(adjustedLD, adjustedLD, -1);
      gl.uniform3fv(programm.nativeProgram.lightingDirectionUniform, adjustedLD);
    }

    gl.uniform3f(programm.nativeProgram.directionalColorUniform, 0.7, 0.7, 0.7);

    gl.uniformMatrix4fv(programm.nativeProgram.pMatrixUniform, false, camera.perspectiveMatrix);
    gl.uniformMatrix4fv(programm.nativeProgram.mvMatrixUniform, false, this.translationMatrix);

    mat3.normalFromMat4(this.normalMatrix, this.translationMatrix);

    gl.uniformMatrix3fv(programm.nativeProgram.nMatrixUniform, false, this.normalMatrix);

    // this._vertexIndicesBuffer.bind();
    if(this.objFileName != "") {
      bufferHandler.get(this.objFileName, VERTEX_INDICES).bind();
      gl.drawElements(this._renderType, bufferHandler.get(this.objFileName, VERTEX_INDICES).items, gl.UNSIGNED_SHORT, 0);
    } else {
      this._vertexIndicesBuffer.bind();
      gl.drawElements(this._renderType, this._vertexIndicesBuffer.items, gl.UNSIGNED_SHORT, 0);
    }
  }

  _checkValidation() {
    // if (this._vertexBuffer == null) {
    //   return false;
    // }
    // if (this._vertexIndicesBuffer == null) {
    //   return false;
    // }
    // if (this._vertexNormalBuffer == null) {
    //   return false;
    // }
    return true;
  }

  _bindBuffers() {
    /* bind vertex buffer */
    if (this.objFileName != "") {
      bufferHandler.get(this.objFileName, VERTEX).bind();
      gl.vertexAttribPointer(programm.nativeProgram.vertexPositionAttribute, bufferHandler.get(this.objFileName, VERTEX).itemSize, gl.FLOAT, false, 0, 0);
    } else {
      this._vertexBuffer.bind();
      gl.vertexAttribPointer(programm.nativeProgram.vertexPositionAttribute, this._vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);
    }

    /* bind normals */

    if (this.objFileName != "") {
      bufferHandler.get(this.objFileName, NORMALS).bind();
      gl.vertexAttribPointer(programm.nativeProgram.vertexNormalAttribute, bufferHandler.get(this.objFileName, NORMALS).itemSize, gl.FLOAT, false, 0, 0);
    } else {
      this._vertexNormalBuffer.bind();
      gl.vertexAttribPointer(programm.nativeProgram.vertexNormalAttribute, this._vertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);
    }


    gl.uniform1i(programm.nativeProgram.useColor, false);
    /* check if a valid texture is available */
    if ((this._texture == null) || (this._textureCoordinatesBuffer == null)) {
      /* if no texture is valid, disable all shader attributes */
      gl.uniform1i(programm.nativeProgram.useTexture, false);
      gl.disableVertexAttribArray(programm.nativeProgram.textureCoordAttribute);

    } else {
      /* bind texture */
      gl.uniform1i(programm.nativeProgram.useTexture, true);
      gl.enableVertexAttribArray(programm.nativeProgram.textureCoordAttribute);

      /* bind texture coordinates */
      // this._textureCoordinatesBuffer.bind();
      // bufferHandler.bindUVBuffer(this.objFileName);
      if (this.objFileName != "") {
        bufferHandler.get(this.objFileName, UV).bind();
        gl.vertexAttribPointer(programm.nativeProgram.textureCoordAttribute, bufferHandler.get(this.objFileName, UV).itemSize, gl.FLOAT, false, 0, 0);
      } else {
        this._textureCoordinatesBuffer.bind();
        gl.vertexAttribPointer(programm.nativeProgram.textureCoordAttribute, this._textureCoordinatesBuffer.itemSize, gl.FLOAT, false, 0, 0);
      }

      /* bind texture */
      // this._texture.bind();
      if (this._textureName != "") {
        textureHandler.get(this._textureName).bind();
      } else {
        this._texture.bind();
      }
      gl.uniform1i(programm.nativeProgram.samplerUniform, 0);
    }
  }

  _matrixCalculations() {
    /* set position */
    mat4.translate(this.translationMatrix, camera.translationMatrix, [this.position.x, this.position.y, this.position.z]);
    mat4.rotate(this.translationMatrix, this.translationMatrix, Radian(this.rotation.x), [1, 0, 0]);
    mat4.rotate(this.translationMatrix, this.translationMatrix, Radian(this.rotation.y), [0, 1, 0]);
    mat4.rotate(this.translationMatrix, this.translationMatrix, Radian(this.rotation.z), [0, 0, 1]);
  }

  static generateColorByID(id) {
    return new feColor(((id % 256.0) / 255.0) * 255, (Math.floor(id / 256.0) % 256.0) * 255, 0, 1);
  }
  static generateIDFromColor(color) {

    return
  }

  _renderPickingBuffer() {
    pickingFBO.bind();

    this.draw();

    /* get clicked pixel */

    pickingFBO.unbind();
  }
}
