class feLight extends feEntity {
  constructor() {
    super();
    this.r = 1.0;
    this.g = 1.0;
    this.b = 1.0;
    this.position = new feVector3D();
    this.direction = new feVector3D();
    this.strenght = 1;
    this.isActive = true;
  }
  enable() {
    this.isActive = true;
  }
  disable() {
    this.isActive = false;
  }
  setPosition(position) {
    this.position = position;
  }
  setRGB(r, g, b) {
    this.r = r;
    this.g = g;
    this.b = b;
  }
  setDirection(vector) {
    this.direction = vector;
  }
}