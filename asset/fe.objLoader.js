class feobjLoader {
  constructor() {
    feEventHandler.attach(this);
    this.fileLoader = new feFileLoader();

    this.fileLoader.on("finished", function () {
      objLoader.parseFile("cube.obj");
      objLoader.trigger("finished");
    });
    this.objects = null;
  }
  addFile(fileName) {
    this.fileLoader.addFile(fileName);
  }
  load() {
    this.fileLoader.load();
  }

  parseFile(fileName) {
    var text = this.fileLoader.content(fileName);
    var arrLines = text.split("\n");
    var lines = arrLines.length;
    /* out */
    var vertices = [];
    var normals = [];
    var uvCoordinates = [];

    var normalIndices = [];
    var vertexIndices = [];
    var uvIndices = [];

    var tmp_vertices = [];
    var tmp_normals = [];
    var tmp_vertexTextureCoordinates = [];

    var splits = [];
    for (var i = 0; i < lines; i++) {
      splits = arrLines[i].split(" ");

      if (splits[0] == "v") {
        var vertex = new feVector3D(splits[1], splits[2], splits[3]);
        tmp_vertices.push(vertex);
      } else if (splits[0] == "vt") {
        var vertex = new feVector3D(splits[1], splits[2]);
        tmp_vertexTextureCoordinates.push(vertex);
      } else if (splits[0] == "vn") {
        var vertex = new feVector3D(splits[1], splits[2], splits[3]);
        tmp_normals.push(vertex);
      } else if (splits[0] == "f") {

        var vertexIndex = [];
        var uvIndex = [];
        var normalIndex = [];
        var split = [];

        for (var j = 1; j <= 3; j++) {
          split = splits[j].split("/");
          vertexIndex.push(split[0]);
          uvIndex.push(split[1]);
          normalIndex.push(split[2]);
        }

        vertexIndices.push(vertexIndex[0]);
        vertexIndices.push(vertexIndex[1]);
        vertexIndices.push(vertexIndex[2]);

        uvIndices.push(uvIndex[0]);
        uvIndices.push(uvIndex[1]);
        uvIndices.push(uvIndex[2]);

        normalIndices.push(normalIndex[0]);
        normalIndices.push(normalIndex[1]);
        normalIndices.push(normalIndex[2]);

      }
    }

    for (var i = 0; i < vertexIndices.length; i++) {
      var index = vertexIndices[i];
      vertices.push(tmp_vertices[index - 1].x); 
      vertices.push(tmp_vertices[index - 1].y);
      vertices.push(tmp_vertices[index - 1].z);
    }

    for (var i = 0; i < uvIndices.length; i++) {
      var index = uvIndices[i];

      uvCoordinates.push(tmp_vertexTextureCoordinates[index - 1].x);
      uvCoordinates.push(tmp_vertexTextureCoordinates[index - 1].y);
    }

    for (var i = 0; i < normalIndices.length; i++) {
      var index = normalIndices[i];

      normals.push(tmp_normals[index - 1].x);
      normals.push(tmp_normals[index - 1].y);
      normals.push(tmp_normals[index - 1].z);
    }

    this.object = { "vertice": vertices, "normals": normals, "uv": uvCoordinates, "vertexIndices": vertexIndices, "uvIndices": uvIndices, "normalIndices": normalIndices };
  }

  getBuffers(fileName) {
    return this.object;
  }
}
