class feLine extends feEntity {
  constructor(vector3Dstart, vector3DEnd, color) {
    super();
    this.translationMatrix = mat4.create();
    this.normalMatrix = mat3.create();

    this.begin = vector3Dstart;
    this.end = vector3DEnd;

    this.points = [this.begin.x, this.begin.y, this.begin.z,
    this.end.x, this.end.y, this.end.z];
    this._vertexBuffer = new feBuffer(this.points, 3);
    this.color = new feColor();
    if (color != null) {
      this.setColor(color);
    }
  }
  setColor(color) {
    this.color = color;
  }
  draw() {

    lineShaderProgram.use();

    this._vertexBuffer.bind();
    gl.vertexAttribPointer(lineShaderProgram.nativeProgram.vertexPositionAttribute, this._vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

    mat4.translate(this.translationMatrix, camera.translationMatrix, [0, 0, 0]);
    gl.uniformMatrix4fv(lineShaderProgram.nativeProgram.pMatrixUniform, false, camera.perspectiveMatrix);
    gl.uniformMatrix4fv(lineShaderProgram.nativeProgram.mvMatrixUniform, false, this.translationMatrix);
    // gl.uniformMatrix3fv(lineShaderProgram.nativeProgram.nMatrixUniform, false, this.normalMatrix);

    gl.uniform4f(lineShaderProgram.nativeProgram.uColor, this.color.r, this.color.g, this.color.b, this.color.a);
    gl.drawArrays(gl.LINES, 0, this._vertexBuffer.items);
  }

  update(dt) {

  }
}