class feWorldAxis {
  constructor() {
    this.xAxisLine = new feLine(new feVector3D(0,0,0), new feVector3D(1,0,0),new feColor(0,0,255));
    this.yAxisLine = new feLine(new feVector3D(0,0,0), new feVector3D(0,1,0),new feColor(0,255,0));
    this.zAxisLine = new feLine(new feVector3D(0,0,0), new feVector3D(0,0,-1),new feColor(255,0,0));
  }
  enable() {
    world.add(this.xAxisLine);
    world.add(this.yAxisLine);
    world.add(this.zAxisLine);
  }
  disable() {
    world.remove(this.xAxisLine);
    world.remove(this.yAxisLine);
    world.remove(this.zAxisLine);
  }
}