class feWorldGrid {
  constructor(rows, cols) {
    var beginX = -(rows / 2);
    var endX = rows / 2;

    var beginZ = -(cols / 2);
    var endZ = cols / 2;

    for (var x = beginX; x <= endX; x++) {
      var line = new feLine(new feVector3D(x, 0, beginZ), new feVector3D(x, 0, endZ), new feColor(128, 128, 128));
      world.add(line);
    }

    for (var x = beginZ; x <= endZ; x++) {
      var line = new feLine(new feVector3D(beginZ, 0, x), new feVector3D(endZ, 0, x), new feColor(128, 128, 128));
      world.add(line);
    }

  }
}