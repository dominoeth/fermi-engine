class feParticleSystem extends feEntity {
  constructor() {
    super();
    this.particles = [];
    this.particleEmitter = new feParticleEmitter();

    this.MAX_NR_OF_PARTICLES = Number.MAX_VALUE;
    this.MAX_NR_OF_EMITS = 1;
    
  }

  update(dt) {
    this.particleEmitter.emit(dt);
    var particle = new feParticle();
    this.particles.push(particle);

    var i = this.particles.length - 1;
    for(i; i >= 0 ; --i) {
      this.particles[i].update(dt);
    }
  }
  clear() {
    var i = this.particles.length - 1;
    for(i; i >= 0 ; --i) {
      this.particles[i].destroy();
    }
  } 
  draw() {
    var i = this.particles.length - 1;
    if(!i) return;
    for(i; i >= 0 ; --i) {
      this.particles[i].draw();
    }
  }

}