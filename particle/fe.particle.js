class feParticle {
  constructor() {
    this.lifeTimer = 1.0;
    this.texture = null;
    this.asset = new feCube();
    this.direction = new feVector3D(Math.random() * 2 - 1,Math.random() * 2 - 1,Math.random() * 2 - 1);
  }
  destroy() {

  }
  draw() {
    this.asset.draw();
  }
  update(dt) {
    var z = this.asset.position;
    z.x += this.direction.x * 5 * dt;
    z.y += this.direction.y * 5 * dt;
    z.z += this.direction.z * 5 * dt;
    this.asset.setPosition(z);
  }
}