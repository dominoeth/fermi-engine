/* this class is for performance measurement and more */
class feFileLoader {
  constructor() {
    feEventHandler.attach(this);
    this.fileName = null;
    // this.xhr = new XMLHttpRequest();
    this.isFinished = false;
    this.filesToLoad = [];
    this.filesLoaded = 0;
  }

  addFile(name) {
    for(var i = 0; i < this.filesToLoad.length; i ++) {
      if(this.filesToLoad[i].name == name) {
        /* file is already in list and have not to be loaded */
        return;
      }
    }
    var file = { name: name, content: "", status: "pending" };
    this.filesToLoad.push(file);
  }

  load() {
    for (var i = 0; i < this.filesToLoad.length; i++) {
      if(this.filesToLoad[i].status == "pending") {
        this._loadText(this.filesToLoad[i].name);
      }
    }
  }

  content(from) {
    if (from != undefined) {
      var text;
      for (var i = 0; i < this.filesToLoad.length; i++) {
        if (this.filesToLoad[i].name == from) {
          if (this.filesToLoad[i].status == "finished") {
            return this.filesToLoad[i].content;
          }
        }
      }
    }
  }

  _checkForFinish() {
    if(this.filesToLoad.length == this.filesLoaded) {
      this.trigger("finished");
    }
  }

  _loadText(filename) {
    var self = this;
    var xhr = new XMLHttpRequest();
    xhr.open('GET', filename);
    xhr.onreadystatechange = function (e) {
      var status = e.target.readyState;
      if (status === 4) {
        console.log("file " + filename + " loaded.");
        self._receiveContent(filename,e.currentTarget.responseText);
      }
    }
    xhr.send();
  }

  _receiveContent(filename, content) {
    for (var i = 0; i < this.filesToLoad.length; i++) {
      if (this.filesToLoad[i].name == filename) {
        this.filesToLoad[i].content = content;
        this.filesToLoad[i].status = "finished";
      }
    }
    this.filesLoaded += 1;
    this._checkForFinish();
  }
}



