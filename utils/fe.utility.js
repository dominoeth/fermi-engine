/* this class is for performance measurement and more */
class feUtility {
  constructor()
  {
    this.deltaTime = 0;
    this.startTime = 0;
    this.timeDiff = 0;
    this.time = 0;
    this.now = 0;

    this.avgArray = [50];
    this.avgIndex = 0;

    this.avg = 0;
    this.min = 0;
    this.max = 0;

    this.currentFrameTime = 0;
    this.currentLoopTime = 0;
    this.lastLoopTime = 0;
    this.frameTime = 0;

    this.fps = 0;
  }
  update()
  {
    this.now = new Date().getTime();
    this.deltaTime = (this.now - (this.time || this.now)) / 1000;
    this.time = this.now;

    this.currentFrameTime = (this.currentLoopTime =new Date) - this.lastLoopTime;
    this.frameTime += this.currentFrameTime - this.frameTime;
    this.lastLoopTime = this.currentLoopTime;

    this.fps = 1000 / this.frameTime;

    /* AVG calculation */
    this.avgArray[++this.avgIndex % 50] = this.fps;
  }
  getFPSCount()
  {
    return this.fps;
  }
  getDeltaTime()
  {
    return this.deltaTime;
  }
  start() {
    this.startTime = new Date().getTime();
  }
  stop() {
    this.timeDiff = (new Date().getTime() - this.startTime) / 1000;
  }

  getMinFPS() {
    return Math.min(...this.avgArray);
  }
  getMaxFPS() {
    return Math.max(...this.avgArray);
  }
  getMeanFPS() {
    var sum = this.avgArray.reduce(function(pv, cv) { return pv + cv; }, 0);
    return sum / 50;
  }
}
