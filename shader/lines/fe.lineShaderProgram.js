class feLineShaderProgram {
  constructor() {
    this.nativeProgram = gl.createProgram();
    this.nrOfShader = 0;
  }

  attachShader(shader) {
    if (!shader instanceof feShader) {
      console.log("can not attach shader. Invald input.");
      return false;
    }

    if (!shader.isCompiled) {
      console.log("can not attach shader. Shader is not compiled");
      return false;
    }

    gl.attachShader(this.nativeProgram, shader.nativeShader);

    this.nrOfShader++;
    return true;
  }

  link() {
    gl.linkProgram(this.nativeProgram);

    if (!gl.getProgramParameter(this.nativeProgram, gl.LINK_STATUS)) {
      console.log("can not link shader to program");
      return false;
    }
    this.use();
    
    this._setupAttributes();

    this._setupUniforms();

    return true;
  }

  use() {
    gl.useProgram(this.nativeProgram);
  }

  _setupAttributes() {
    this.nativeProgram.vertexPositionAttribute = gl.getAttribLocation(this.nativeProgram, "aVertexPosition");
    gl.enableVertexAttribArray(this.nativeProgram.vertexPositionAttribute);
  }

  _setupUniforms() {
    this.nativeProgram.pMatrixUniform = gl.getUniformLocation(this.nativeProgram, "uPMatrix");
    this.nativeProgram.mvMatrixUniform = gl.getUniformLocation(this.nativeProgram, "uMVMatrix");
    this.nativeProgram.nMatrixUniform = gl.getUniformLocation(this.nativeProgram, "uNMatrix");
    this.nativeProgram.uColor = gl.getUniformLocation(this.nativeProgram, "uColor");
  }
}
