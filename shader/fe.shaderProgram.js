class feShaderProgram {
  constructor() {
    this.nativeProgram = gl.createProgram();
    this.nrOfShader = 0;
  }

  attachShader(shader) {
    if (!shader instanceof feShader) {
      console.log("can not attach shader. Invald input.");
      return false;
    }

    if (!shader.isCompiled) {
      console.log("can not attach shader. Shader is not compiled");
      return false;
    }

    gl.attachShader(this.nativeProgram, shader.nativeShader);

    this.nrOfShader++;
    return true;
  }

  link() {
    gl.linkProgram(this.nativeProgram);

    if (!gl.getProgramParameter(this.nativeProgram, gl.LINK_STATUS)) {
      console.log("can not link shader to program");
      return false;
    }
    this.use();
    
    this._setupAttributes();

    this._setupUniforms();

    return true;
  }

  use() {
    gl.useProgram(this.nativeProgram);
  }

  _setupAttributes() {
    this.nativeProgram.vertexPositionAttribute = gl.getAttribLocation(this.nativeProgram, "aVertexPosition");
    gl.enableVertexAttribArray(this.nativeProgram.vertexPositionAttribute);

    this.nativeProgram.textureCoordAttribute = gl.getAttribLocation(this.nativeProgram, "aTextureCoord");
    gl.enableVertexAttribArray(this.nativeProgram.textureCoordAttribute);

    this.nativeProgram.vertexNormalAttribute = gl.getAttribLocation(this.nativeProgram, "aVertexNormal");
    gl.enableVertexAttribArray(this.nativeProgram.vertexNormalAttribute);

    gl.uniform1i(programm.nativeProgram.useColor, false);

  }

  _setupUniforms() {
    this.nativeProgram.pMatrixUniform = gl.getUniformLocation(this.nativeProgram, "uPMatrix");
    this.nativeProgram.mvMatrixUniform = gl.getUniformLocation(this.nativeProgram, "uMVMatrix");
    this.nativeProgram.nMatrixUniform = gl.getUniformLocation(this.nativeProgram, "uNMatrix");
    this.nativeProgram.samplerUniform = gl.getUniformLocation(this.nativeProgram, "uSampler");
    this.nativeProgram.useLightingUniform = gl.getUniformLocation(this.nativeProgram, "uUseLighting");
    this.nativeProgram.ambientColorUniform = gl.getUniformLocation(this.nativeProgram, "uAmbientColor");
    this.nativeProgram.lightingDirectionUniform = gl.getUniformLocation(this.nativeProgram, "uLightingDirection");
    this.nativeProgram.directionalColorUniform = gl.getUniformLocation(this.nativeProgram, "uDirectionalColor");
    this.nativeProgram.pointLightingLocation = gl.getUniformLocation(this.nativeProgram, "uPointLightingLocation");
    this.nativeProgram.pointLightingColor = gl.getUniformLocation(this.nativeProgram, "uPointLightingColor");
    this.nativeProgram.useTexture = gl.getUniformLocation(this.nativeProgram, "uUseTextures");
    this.nativeProgram.useColor = gl.getUniformLocation(this.nativeProgram, "uUseColor");
    this.nativeProgram.uColor = gl.getUniformLocation(this.nativeProgram, "uColor");
  }
}
