class feShader {
  constructor() {
    this.nativeShader = null;
    this.text = null;
    this.type = null;
    this.isCompiled = false;
  }

  setShaderCode(code) {
    if (code != undefined) {
      this.text = code;
    }
  }

  setType(type) {
    if (type != undefined) {
      this.type = type;
      this.nativeShader = gl.createShader(type);
    }
  }

  compile() {
    if (this.nativeShader == null) {
      return false;
    }

    gl.shaderSource(this.nativeShader, this.text);
    gl.compileShader(this.nativeShader);

    if (!gl.getShaderParameter(this.nativeShader, gl.COMPILE_STATUS)) {
      console.log(gl.getShaderInfoLog(this.nativeShader));
      this.isCompiled = false;
      return false;
    }
    this.isCompiled = true;
    return true;
  }

}
