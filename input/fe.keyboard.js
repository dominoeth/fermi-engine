class feKeyboard {
  constructor() {
    feEventHandler.attach(this);
  }
  onKeyDown(key, callback) {
    var currentKey = new feKey(key);
    document.addEventListener('keydown', function (event) {
      if (event.keyCode == currentKey.getKeyCode()) {
        callback();
      }
    });
  }
  onKeyPress(key, callback) {
    var currentKey = new feKey(key);
    document.addEventListener('keydown', function (event) {
      if (event.keyCode == currentKey.getKeyCode()) {
        callback();
      }
    });
  }
  onKeyUp(key, callback) {
    var currentKey = new feKey(key);
    document.addEventListener('keyup', function (event) {
      if (event.keyCode == currentKey.getKeyCode()) {
        callback();
      }
    });
  }
}

class feKey {
  constructor(key) {
    this.key = key;
  }
  getKeyCode() {
    switch (this.key) {
      case "a":
        return 65;
      case "b":
        return 66;
      case "c":
        return 67;
      case "d":
        return 68;
      case "e":
        return 69;
      case "f":
        return 70;
      case "g":
        return 71;
      case "h":
        return 72;
      case "i":
        return 73;
      case "j":
        return 74;
      case "k":
        return 75;
      case "l":
        return 76;
      case "m":
        return 77;
      case "n":
        return 78;
      case "o":
        return 79;
      case "p":
        return 80;
      case "q":
        return 81;
      case "r":
        return 82;
      case "s":
        return 83;
      case "t":
        return 84;
      case "u":
        return 85;
      case "v":
        return 86;
      case "w":
        return 87;
      case "x":
        return 88;
      case "y":
        return 89;
      case "z":
        return 90;
      case "ctrl":
        return 17;
      case "shift":
        return 16;
      case "space":
        return 32;
      case "up":
        return 38;
      case "down":
        return 40;
      case "left":
        return 37;
      case "right":
        return 39;
    }

  }
}