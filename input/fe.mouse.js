class feMouse {
  constructor() {
    feEventHandler.attach(this);
    var self = this;
    document.body.addEventListener("mousemove", function (event) {
      self.move(event);
    });

    document.body.addEventListener("mousedown", function (event) {
      self.mouseDown = true;
    });

    document.body.addEventListener("mouseup", function (event) {
      self.mouseDown = false;
    });

    this.deltaX = 0;
    this.deltaY = 0;
    this.x = 0;
    this.y = 0;

    this.lastX = 0;
    this.lastY = 0;

    this.mouseDown = false;
  }
  getScreenPosition() {
    return new feVector2D(this.x, this.y);
  }
  update() {
    this.trigger("update");
  }
  move(event) {


    if (event.movementX != 0) {

      this.deltaX = -event.movementX;
      this.deltaY = -event.movementY;

    } else {

      this.x = event.clientX;
      this.y = event.clientY;

      this.deltaX = this.lastX - this.x;
      this.deltaY = this.lastY - this.y;

      this.lastX = this.x;
      this.lastY = this.y;

    }

    this.trigger("move");
  }
}

