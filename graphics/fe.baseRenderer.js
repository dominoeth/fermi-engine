class feBaseRenderer {
  constructor() {
    this._domElement = null;
  }

  initialize(DOMElement) {
    if (DOMElement == undefined) {
      return false;
    }
    this._domElement = DOMElement;
    try {
      gl = this._domElement.getContext("experimental-webgl");
      gl.viewportWidth = this._domElement.offsetWidth;
      gl.viewportHeight = this._domElement.offsetHeight;
    } catch (e) {
      console.log(e);
    }
    if (!gl) {
      return false;
    }

    mat4.perspective(pMatrix, 70 * Math.PI / 180, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0);
    mat4.identity(mvMatrix);
    mat4.translate(mvMatrix, mvMatrix, [-1.0, 0, -5]);
    return true;
  }

  initShader() {

    var shader = new feShader();
    shader.setShaderCode(loader.content("../../fermi-engine/shader/fe.basicFragmentShader.fs"));
    shader.setType(gl.FRAGMENT_SHADER);
    shader.compile();

    var vertex = new feShader();
    vertex.setShaderCode(loader.content("../../fermi-engine/shader/fe.basicVertexShader.vs"));
    vertex.setType(gl.VERTEX_SHADER);
    vertex.compile();

    programm = new feShaderProgram();
    programm.attachShader(shader);
    programm.attachShader(vertex);
    programm.link();

    /* LINE SHADER */
    var shaderLine = new feShader();
    shaderLine.setShaderCode(loader.content("../../fermi-engine/shader/lines/fe.basicLineFragmentShader.fs"));
    shaderLine.setType(gl.FRAGMENT_SHADER);
    shaderLine.compile();

    var vertexLine = new feShader();
    vertexLine.setShaderCode(loader.content("../../fermi-engine/shader/lines/fe.basicLineVertexShader.vs"));
    vertexLine.setType(gl.VERTEX_SHADER);
    vertexLine.compile();

    lineShaderProgram = new feLineShaderProgram();
    lineShaderProgram.attachShader(shaderLine);
    lineShaderProgram.attachShader(vertexLine);
    lineShaderProgram.link();

    // /* LINE SHADER */
    // var shaderFBO= new feShader();
    // shaderFBO.setShaderCode(loader.content("../../fermi-engine/shader/fbo/fe.basicFBOFragmentShader.fs"));
    // shaderFBO.setType(gl.FRAGMENT_SHADER);
    // shaderFBO.compile();

    // var vertexFBO = new feShader();
    // vertexFBO.setShaderCode(loader.content("../../fermi-engine/shader/fbo/fe.basicFBOVertexShader.vs"));
    // vertexFBO.setType(gl.VERTEX_SHADER);
    // vertexFBO.compile();

    // fboShaderProgram = new feFBOShaderProgram();
    // fboShaderProgram.attachShader(shaderFBO);
    // fboShaderProgram.attachShader(vertexFBO);
  }

  initTextures() {

  }

  initBuffer() {
    pickingFBO = new feFBO(gl.viewportWidth,gl.viewportHeight);
  }

  /* render all objects inside world array */
  render() {
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
    gl.clearColor(1, 1, 1, 1);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    for (var i = 0; i < world.objects.length; i++) {
      world.objects[i].draw();
      // if (world.objects[i] instanceof feAsset) {
      //   world.objects[i]._renderPickingBuffer();
      // }
    }
    // pickingFBO.draw();
    camera.drawDebugLines();
  }
}

var pickingFBO;
var programm;
var lineShaderProgram;
var fboShaderProgram;
var mvMatrix = mat4.create();
var pMatrix = mat4.create();
var rotation = 0;
var mvMatrixStack = [];
