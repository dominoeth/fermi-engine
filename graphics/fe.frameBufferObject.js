class feFBO {
  constructor(width, height) {
    /* create render buffer and allocate it to given size */
    this.width = width;
    this.height = height;
    this.fbo = gl.createFramebuffer();
    this.rtt = gl.createTexture();
    this.renderBuffer = gl.createRenderbuffer();

    // define size and format of level 0
    var level = 0;
    var internalFormat = gl.RGBA;
    var border = 0;
    var format = gl.RGBA;
    var type = gl.UNSIGNED_BYTE;
    var data = null;

    gl.bindTexture(gl.TEXTURE_2D, this.rtt);
    gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, this.width, this.height, border, format, type, data);

    // set the filtering so we don't need mips
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);


    gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);

    // attach the texture as the first color attachment
    const attachmentPoint = gl.COLOR_ATTACHMENT0;
    gl.framebufferTexture2D(gl.FRAMEBUFFER, attachmentPoint, gl.TEXTURE_2D, this.rtt, level);


    // create a depth renderbuffer
    gl.bindRenderbuffer(gl.RENDERBUFFER, this.renderBuffer);
 
    // make a depth buffer and the same size as the targetTexture
    gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, this.width, this.height);
    gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this.renderBuffer);
  }
  bind() {
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);
  }

  unbind() {
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
  }
  clear() {
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);
    gl.viewport(0, 0, this.width, this.height);
    gl.clearColor(0, 0, 0, 1); // red
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  }

  readPixel(x, y) {
    if (x >= this.width || y >= this.height) {
      console.warn("cannot read pixel from fbo! Out of bounds");
      return -1;
    }
    this.bind();

    var colorPicked = new Uint8Array(4);
    gl.readPixels(x, y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, colorPicked);
    this.unbind();

    return colorPicked;
  }
}