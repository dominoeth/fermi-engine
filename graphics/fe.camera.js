class feCamera {
  constructor() {
    feEventHandler.attach(this);
    var self = this;
    this.perspectiveMatrix = mat4.create();
    this.translationMatrix = mat4.create();
    this.viewMatrix = mat4.create();

    this.position = new feVector3D();
    this.rotation = new feVector3D();
    this.yaw = 0;
    this.pitch = 0;
    this.roll = 0;

    this.upVector = new feVector3D(0, 1, 0);
    this.viewVector = new feVector3D(0, 0, -1);
    this.rightVector = new feVector3D(1, 0, 0);

    this.quat = new feQuaternion();

    /*  */
    this.target = new feVector3D();
    this.up = new feVector3D(0, 0, 1);
    this.direction = normalize(subtract(this.position, this.target));
    this.right = cross(this.direction, this.up);








    this.moveForward = false;
    this.moveBackward = false;
    this.moveLeft = false;
    this.moveRight = false;
    this.moveUp = false;
    this.moveDown = false;
    this.mouseMove = false;
    this.rotateLeft = false;
    this.rotateRight = false;
    this.rotateUp = false;
    this.rotateDown = false;

    keyboard.onKeyDown("w", function () {
      self.moveForward = true;
    });

    keyboard.onKeyUp("w", function () {
      self.moveForward = false;
    });

    keyboard.onKeyDown("s", function () {
      self.moveBackward = true;
    });

    keyboard.onKeyUp("s", function () {
      self.moveBackward = false;
    });

    keyboard.onKeyDown("a", function () {
      self.moveLeft = true;
    });

    keyboard.onKeyUp("a", function () {
      self.moveLeft = false;
    });

    keyboard.onKeyDown("d", function () {
      self.moveRight = true;
    });

    keyboard.onKeyUp("d", function () {
      self.moveRight = false;
    });

    keyboard.onKeyDown("space", function () {
      self.moveUp = true;
    });

    keyboard.onKeyUp("space", function () {
      self.moveUp = false;
    });

    keyboard.onKeyDown("shift", function () {
      self.moveDown = true;
    });

    keyboard.onKeyUp("shift", function () {
      self.moveDown = false;
    });

    keyboard.onKeyDown("left", function () {
      self.rotateLeft = true;
    });

    keyboard.onKeyUp("left", function () {
      self.rotateLeft = false;
    });

    keyboard.onKeyDown("right", function () {
      self.rotateRight = true;
    });

    keyboard.onKeyUp("right", function () {
      self.rotateRight = false;
    });

    keyboard.onKeyDown("up", function () {
      self.rotateUp = true;
    });

    keyboard.onKeyUp("up", function () {
      self.rotateUp = false;
    });

    keyboard.onKeyDown("down", function () {
      self.rotateDown = true;
    });

    keyboard.onKeyUp("down", function () {
      self.rotateDown = false;
    });
    // mouse.on("move", function () {
    //   self.mouseMove = true;
    // });
  }

  initialize() {
    mat4.perspective(this.perspectiveMatrix, 90 * Math.PI / 180, gl.viewportWidth / gl.viewportHeight, 0.1, 1000.0);
  }

  setPosition(position) {
    this.position = position;
    mat4.translate(this.translationMatrix, this.translationMatrix, [position.x, position.y, position.z]);
  }

  update(dt) {
    var positionChanged = false;
    var pos = new feVector3D();
    var axis = new feVector3D();

    if (this.moveForward) {
      pos.z += dt * 10;
      positionChanged = true;
    }
    if (this.moveBackward) {
      pos.z += dt * -10;
      positionChanged = true;
    }
    if (this.moveLeft) {
      pos.x += dt * 10;
      positionChanged = true;
    }
    if (this.moveRight) {
      pos.x += dt * -10;
      positionChanged = true;
    }
    if (this.moveUp) {
      pos.y += dt * -10;
      positionChanged = true;
    }
    if (this.moveDown) {
      pos.y += dt * 10;
      positionChanged = true;
    }

    if (this.rotateLeft) {
      this.rotateX(dt * 50);
      this.yaw += dt * -5;
      positionChanged = true;
    }
    if (this.rotateRight) {
      this.rotateX(dt * -50);
      this.yaw += dt * 5;
      positionChanged = true;
    }

    if (this.rotateDown) {
      // this.pitch += dt * 5;
      this.rotateY(dt * 50);
      positionChanged = true;
    }

    if (this.rotateUp) {
      // this.pitch += dt * -5;
      this.rotateY(dt * -50);
      positionChanged = true;
    }

    if (positionChanged) {
      mat4.identity(this.translationMatrix);


      this.position = add(this.position, pos);
      // this.position.add(pos);
      mat4.translate(this.translationMatrix, this.translationMatrix, [this.position.x, this.position.y, this.position.z]);
    }
  }

  unwrapAngles() {

  }
  /* yaw */
  rotateX(value) {
    /* rotate around up vector */
    this.viewVector.rotateZ(new feVector3D(), Radian(value));
    this.viewVector.rotateY(new feVector3D(), Radian(value));
    // this.rightVector.rotateZ(new feVector3D(), Radian(value));
  }
  /* pitch */
  rotateY(value) {
    /* rotate around right vector */
    this.viewVector.rotateX(new feVector3D(), Radian(value));
    // this.upVector.rotateY(new feVector3D(), Radian(value));
  }
  /* roll */
  rotateZ(value) {
    /* rotate around viewVector */

  }
  drawDebugLines() {
    /* debug  */
    var center = new feVector3D();
    var lineView = new feLine(center, this.viewVector, new feColor(255, 0, 0));
    var lineUp = new feLine(center, this.upVector, new feColor(0, 255, 0));
    var lineRight = new feLine(center, this.rightVector, new feColor(0, 0, 255));

    lineView.draw();
    lineUp.draw();
    lineRight.draw();
    
  }
}
