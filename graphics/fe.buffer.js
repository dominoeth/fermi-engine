class feBuffer {
  constructor(vertices, size, type, arrayType) {
    this.type = null;
    if (type == undefined) {
      type = gl.ARRAY_BUFFER;
    }
    this.type = type;
    this.vertexPositionBuffer = gl.createBuffer();
    gl.bindBuffer(type, this.vertexPositionBuffer);

    if (vertices != undefined) {
      if (arrayType == undefined) {
        gl.bufferData(type, new Float32Array(vertices), gl.STATIC_DRAW);
      } else if (arrayType == "float32") {
        gl.bufferData(type, new Float32Array(vertices), gl.STATIC_DRAW);
      } else if (arrayType == "uint16") {
        gl.bufferData(type, new Uint16Array(vertices), gl.STATIC_DRAW);
      }

      this.items = vertices.length / size;
      this.itemSize = size;

      this.vertexPositionBuffer.itemSize = size;
      this.vertexPositionBuffer.items = this.items;
    }
  }
  bind() {
    gl.bindBuffer(this.type, this.vertexPositionBuffer);
  }
}
